<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\VipLevelController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});


Route::get('/products', [ProductController::class, 'index'])->name('products.index');
Route::delete('/products/{product}/delete', [ProductController::class, 'delete'])->name('products.delete');
Route::get('/products/create', [ProductController::class, 'create'])->name('products.create');
Route::post('/products/insert', [ProductController::class, 'insert'])->name('products.insert');
Route::get('/products/{product}/confirm-delete', [ProductController::class, 'confirmDelete'])->name('products.confirm-delete');
Route::get('/products/{product}/edit', [ProductController::class, 'edit'])->name('products.edit');
Route::put('/products/{product}/update', [ProductController::class, 'update'])->name('products.update');


Route::get('/clients', [ClientController::class, 'index'])->name('clients.index');
Route::delete('/clients/{client}/delete', [ClientController::class, 'delete'])->name('clients.delete');
Route::get('/clients/create', function () {
    return view('clients.create');
})->name('clients.create');
Route::post('/clients/insert', [ClientController::class, 'insert'])->name('clients.insert');
Route::get('/clients/{client}/confirm-delete', [ClientController::class, 'confirmDelete'])->name('clients.confirm-delete');


Route::get('/orders', [OrderController::class, 'index'])->name('orders.index');
Route::get('/orders/create', [OrderController::class, 'create'])->name('orders.create');
Route::post('/orders/insert', [OrderController::class, 'insert'])->name('orders.insert');
Route::get('/orders/{order}', [OrderController::class, 'show'])->name('orders.show');


Route::get('/categories', [CategoryController::class, 'index'])->name('categories.index');;
Route::get('/categories/create', function () {
    return view('categories.create');
})->name('categories.create');
Route::post('categories/insert', [CategoryController::class, 'insert'])->name('categories.insert');
Route::delete('categories/{category}/delete', [CategoryController::class, 'delete'])->name('categories.delete');
Route::get('categories/{category}/confirm-delete', [CategoryController::class, 'confirmDelete'])->name('categories.confirm-delete');
Route::get('categories/{category}/edit', [CategoryController::class, 'edit'])->name('categories.edit');
Route::put('categories/{category}', [CategoryController::class, 'update'])->name('categories.update');


Route::get('/vip-levels', [VipLevelController::class, 'index'])->name('vip-levels.index');
Route::get('/vip-levels/create', function () {
    return view('vip-levels.create');
})->name('vip-levels.create');
Route::post('/vip-levels/insert', [VipLevelController::class, 'insert'])->name('vip-levels.insert');
Route::get('/vip-levels/{vipLevel}/edit', [VipLevelController::class, 'edit'])->name('vip-levels.edit');
Route::put('/vip-levels/{vipLevel}/update', [VipLevelController::class, 'update'])->name('vip-levels.update');
Route::delete('/vip-levels/{vipLevel}/delete', [VipLevelController::class, 'delete'])->name('vip-levels.delete');

require __DIR__.'/auth.php';
