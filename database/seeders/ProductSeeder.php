<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    // php artisan make:seeder ProductSeeder
    public function run(): void
    {
        DB::table('products')->insert([
            [
                'name' => 'Product 1',
                'quantity' => 100,
                'price' => 10.99,
                'category_id' => 1,
                'description' => 'Description for product 1',
            ],
            [
                'name' => 'Product 2',
                'quantity' => 200,
                'price' => 20.99,
                'category_id' => 2,
                'description' => 'Description for product 2',
            ],
            [
                'name' => 'Product 3',
                'quantity' => 300,
                'price' => 30.99,
                'category_id' => 3,
                'description' => 'Description for product 3',
            ],
        ]);
    }
}
