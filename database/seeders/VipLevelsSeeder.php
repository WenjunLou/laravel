<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VipLevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('vip_levels')->insert([
            ['name' => 'VIP1', 'required_amount' => 0],
            ['name' => 'VIP2', 'required_amount' => 10],
            ['name' => 'VIP3', 'required_amount' => 100],
            ['name' => 'VIP4', 'required_amount' => 1000],
            ['name' => 'VIP5', 'required_amount' => 10000],
            ['name' => 'VIP6', 'required_amount' => 100000],
            ['name' => 'VIP7', 'required_amount' => 1000000],
            ['name' => 'VIP8', 'required_amount' => 10000000],
            ['name' => 'VIP9', 'required_amount' => 100000000],
            ['name' => 'VIP10', 'required_amount' => 1000000000],
        ]);

    }
}
