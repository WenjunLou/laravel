<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('categories')->insert([
            [
                'name' => 'Category 1',
                'iva_percentage' => 21,
            ],
            [
                'name' => 'Category 2',
                'iva_percentage' => 10,
            ],
            [
                'name' => 'Category 3',
                'iva_percentage' => 4,
            ],
        ]);
    }
}
