<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('clients')->insert([
            [
                'first_name' => 'Client',
                'last_name' => 'No1',
                'phone' => '123456789',
                'email' => 'client1@example.com',
                'address' => 'Plaza 123',
            ],
            [
                'first_name' => 'Client',
                'last_name' => 'No2',
                'phone' => '987654321',
                'email' => 'client2@example.com',
                'address' => 'Plaza 456',
            ],
        ]);
    }
}
