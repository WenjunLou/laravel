<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrderProduct extends Model
{
    use HasFactory;

    protected $table = 'order_product';

    protected $fillable = [
        'order_id',
        'product_id',
        'unit_price',
        'iva_percentage',
        'quantity',
        'total_amount',
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
    public $timestamps = false;
}
