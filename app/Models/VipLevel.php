<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class VipLevel extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'required_amount',
    ];

    public function clients(): HasMany
    {
        return $this->hasMany(Client::class);
    }
}
