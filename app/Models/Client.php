<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Client extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'vip_level_id',
        'phone',
        'email',
        'address'
    ];

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    public function vipLevel(): BelongsTo
    {
        return $this->belongsTo(VipLevel::class);
    }

    public function updateVipLevel($totalExpenses): void
    {
        $currentVipLevel = $this->vipLevel;
        $vipLevels = VipLevel::orderBy('required_amount')->get();

        $currentIndex = $vipLevels->search(function ($vipLevel) use ($currentVipLevel) {
            return $vipLevel->id === $currentVipLevel->id;
        });

        $newIndex = $currentIndex;
        foreach ($vipLevels as $index => $vipLevel) {
            if ($totalExpenses >= $vipLevel->required_amount) {
                $newIndex = $index;
            } else {
                break;
            }
        }

        if ($currentIndex !== $newIndex) {
            $newVipLevel = $vipLevels[$newIndex];
            $this->vip_level_id = $newVipLevel->id;
            $this->save();
        }
    }
}
