<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('categories.index', compact('categories'));
    }

    public function insert(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'iva_percentage' => 'required|numeric|min:0|max:100',
        ]);

        Category::create([
            'name' => $request->name,
            'iva_percentage' => $request->iva_percentage,
        ]);

        return redirect()->route('categories.index')->with('success', 'Category created successfully.');
    }

    public function confirmDelete(Category $category)
    {
        return view('categories.confirm-delete', compact('category'));
    }

    public function delete(Request $request, Category $category)
    {
        $category->delete();
        return redirect()->route('categories.index')->with('success', 'Category deleted successfully.');
    }

    public function edit(Category $category)
    {
        return view('categories.edit', compact('category'));
    }

    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name' => 'required',
            'iva_percentage' => 'required|numeric|min:0|max:100',
        ]);

        $category->update([
            'name' => $request->name,
            'iva_percentage' => $request->iva_percentage,
        ]);

        return redirect()->route('categories.index')->with('success', 'Category updated successfully.');
    }
}
