<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function index()
    {
        $orders = Order::with(['client', 'products'])->get();
        return view('orders.index', compact('orders'));
    }

    public function create()
    {
        $clients = Client::all();
        $products = Product::all();
        return view('orders.create', compact('clients', 'products'));
    }

    public function insert(Request $request)
    {
        $request->validate([
            'client_id' => 'required|exists:clients,id',
            'products' => 'required|array',
            'products.*' => 'exists:products,id',
            'quantities' => 'required|array',
            'quantities.*' => 'integer|min:0',
        ]);

        $hasValidProduct = false;
        $totalAmountBeforeIva = 0;
        $ivaAmount = 0;
        $insufficientStock = [];

        foreach ($request->products as $index => $productId) {
            $quantity = $request->quantities[$index];
            if ($quantity > 0) {
                $product = Product::findOrFail($productId);
                if ($product->quantity < $quantity) {
                    $insufficientStock[] = $product->name;
                } else {
                    $hasValidProduct = true;
                    $totalAmountBeforeIva += $product->price * $quantity;
                    $ivaAmount += ($product->price * $quantity) * ($product->category->iva_percentage / 100);
                }
            }
        }

        if (!empty($insufficientStock)) {
            return redirect()->route('orders.create')->with('error', 'The following products have insufficient stock: ' . implode(', ', $insufficientStock));
        }

        if (!$hasValidProduct) {
            return redirect()->route('orders.create')->with('error','The order must contain at least one product with a quantity greater than 0.');
        }

        $totalAmountAfterIva = $totalAmountBeforeIva + $ivaAmount;

        $orderDate = now();
        $orderNumber = $orderDate->format('YmdHis');

        $order = Order::create([
            'client_id' => $request->client_id,
            'order_date' => $orderDate,
            'order_number' => $orderNumber,
            'total_amount' => $totalAmountAfterIva,
            'total_amount_before_iva' => $totalAmountBeforeIva,
            'iva_amount' => $ivaAmount,
        ]);

        foreach ($request->products as $index => $productId) {
            $quantity = $request->quantities[$index];
            if ($quantity > 0) {
                $product = Product::findOrFail($productId);

                $order->products()->attach($productId, [
                    'quantity' => $quantity,
                    'unit_price' => $product->price,
                    'iva_percentage' => $product->category->iva_percentage,
                    'total_amount' => ($product->price * $quantity) + (($product->price * $quantity) * ($product->category->iva_percentage / 100)),
                ]);

                $product->decrement('quantity', $quantity);
            }
        }

        $client = Client::findOrFail($request->client_id);
        $totalExpenses = $client->orders()->sum('total_amount');
        $client->updateVipLevel($totalExpenses);

        return redirect()->route('orders.index')->with('success', 'Order created successfully');
    }

    public function show(Order $order)
    {
        return view('orders.show', compact('order'));
    }
}
