<?php

namespace App\Http\Controllers;

use App\Models\VipLevel;
use Illuminate\Http\Request;

class VipLevelController extends Controller
{
    public function index()
    {
        $vipLevels = VipLevel::orderBy('required_amount', 'asc')->get();
        return view('vip-levels.index', compact('vipLevels'));
    }

    public function insert(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:vip_levels,name',
            'required_amount' => 'required|numeric|min:0',
        ]);

        VipLevel::create($request->all());

        return redirect()->route('vip-levels.index')->with('success', 'VIP Level created successfully.');
    }

    public function edit(VipLevel $vipLevel)
    {
        return view('vip-levels.edit', compact('vipLevel'));
    }

    public function update(Request $request, VipLevel $vipLevel)
    {
        $request->validate([
            'name' => 'required|unique:vip_levels,name,' . $vipLevel->id,
            'required_amount' => 'required|integer|min:0',
        ]);

        $vipLevel->update($request->all());

        return redirect()->route('vip-levels.index')->with('success', 'VIP Level updated successfully.');
    }

}
