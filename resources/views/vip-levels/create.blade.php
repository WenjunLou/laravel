<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create VIP Level</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create VIP Level') }}
        </h2>
    </x-slot>
    <div class="container mt-5">
        <form action="{{ route('vip-levels.insert') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" required>
            </div>
            <div class="form-group">
                <label for="required_amount">Required Amount</label>
                <input type="number" class="form-control" id="required_amount" name="required_amount" required>
            </div>
            <button type="submit" class="btn btn-primary">Create VIP Level</button>
        </form>
    </div>
</x-app-layout>
</body>
</html>
