<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit VIP Level</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit VIP Level') }}
        </h2>
    </x-slot>
    <div class="container mt-5">
        <form action="{{ route('vip-levels.update', $vipLevel->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $vipLevel->name }}" required>
            </div>
            <div class="form-group">
                <label for="required_amount">Required Amount</label>
                <input type="number" class="form-control" id="required_amount" name="required_amount" value="{{ $vipLevel->required_amount }}" required>
            </div>
            <button type="submit" class="btn btn-primary">Update VIP Level</button>
        </form>
    </div>
</x-app-layout>
</body>
</html>
