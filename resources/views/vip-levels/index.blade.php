<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VIP Levels</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('VIP Levels') }}
        </h2>
    </x-slot>
    <div class="container mt-5">
        <h1>VIP Levels</h1>
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Name</th>
                <th>Required Amount</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($vipLevels as $vipLevel)
                <tr>
                    <td>{{ $vipLevel->name }}</td>
                    <td>{{ $vipLevel->required_amount }}</td>
                    <td>
                        <a href="{{ route('vip-levels.edit', $vipLevel->id) }}" class="btn btn-info">Edit</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <a href="{{ route('vip-levels.create') }}" class="btn btn-primary">Add VIP Level</a>
    </div>
</x-app-layout>
</body>
</html>
