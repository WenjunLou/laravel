<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Category List</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Categories') }}
        </h2>
    </x-slot>
<div class="container mt-5">
    <h1>Category List</h1>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <div class="row">
        <div class="col">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Category Name</th>
                    <th>IVA Percentage</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($categories as $category)
                    <tr>
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->iva_percentage }}%</td>
                        <td>
                            <a href="{{ route('categories.edit', ['category' => $category]) }}" class="btn btn-info">Edit</a>
                            <a href="{{ route('categories.confirm-delete', ['category' => $category]) }}" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <a href="{{ route('categories.create') }}" class="btn btn-primary">Add Category</a>
        </div>
    </div>
</div>
</x-app-layout>
</body>
</html>
