<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Category</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Category') }}
        </h2>
    </x-slot>
<div class="container mt-5">
    <form action="{{ route('categories.update', ['category' => $category]) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Category Name:</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ $category->name }}" required>
        </div>
        <div class="form-group">
            <label for="iva_percentage">IVA Percentage:</label>
            <input type="number" class="form-control" id="iva_percentage" name="iva_percentage" value="{{ $category->iva_percentage }}" required min="0" max="100" step="0.01">
        </div>
        <button type="submit" class="btn btn-primary">Update Category</button>
    </form>
</div>
</x-app-layout>
</body>
</html>
