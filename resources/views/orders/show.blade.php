<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Order Details</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Order Details') }}
        </h2>
    </x-slot>
<div class="container mt-5">
    <div class="card">
        <div class="card-header">
            Order ID: {{ $order->id }}
        </div>
        <div class="card-body">
            <h5 class="card-title">Client: {{ $order->client->first_name }} {{ $order->client->last_name }}</h5>
            <p class="card-text">Order Date: {{ $order->order_date }}</p>
            <p class="card-text">Total Amount: ${{ $order->total_amount }}</p>
            <h5>Products:</h5>
            <table class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Unit Price</th>
                    <th>Quantity</th>
                    <th>Total Price Before IVA</th>
                    <th>IVA</th>
                    <th>Total Amount</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($order->products as $product)
                    <tr>
                        <td>{{ $product->name }}</td>
                        <td>${{ $product->price }}</td>
                        <td>{{ $product->pivot->quantity }}</td>
                        <td>${{ $product->price * $product->pivot->quantity }}</td>
                        <td>{{ $product->category->iva_percentage }}%</td>
                        <td>${{ $product->pivot->total_amount }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</x-app-layout>
</body>
</html>
