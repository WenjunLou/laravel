<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Order List</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Orders') }}
        </h2>
    </x-slot>
<div class="container mt-5">
    <h1>Order List</h1>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Order Number</th>
            <th>Client</th>
            <th>Products</th>
            <th>Amount Before IVA</th>
            <th>IVA Amount</th>
            <th>Total Amount</th>
            <th>Order Date</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($orders as $order)
            <tr>
                <td>{{ $order->order_number }}</td>
                <td>{{ $order->client->first_name }} {{ $order->client->last_name }}</td>
                <td>
                    <ul>
                        @foreach ($order->products as $product)
                            <li>{{ $product->name }} ({{ $product->pivot->quantity }})</li>
                        @endforeach
                    </ul>
                </td>
                <td>${{ $order->total_amount_before_iva }}</td>
                <td>${{ $order->iva_amount }}</td>
                <td>${{ $order->total_amount }}</td>
                <td>{{ $order->order_date }}</td>
                <td>
                    <a href="{{ route('orders.show', $order->id) }}" class="btn btn-info">Order Details</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <a href="{{ route('orders.create') }}" class="btn btn-primary">Add Order</a>
</div>
</x-app-layout>
</body>
</html>
