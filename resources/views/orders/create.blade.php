<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Order</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <script>
        document.addEventListener('DOMContentLoaded', (event) => {
            document.querySelectorAll('input[name="quantities[]"]').forEach(input => {
                input.addEventListener('input', function() {
                    const row = this.closest('tr');
                    const unitPrice = parseFloat(row.querySelector('input[name="prices[]"]').value);
                    const quantity = parseInt(this.value);
                    const ivaPercentage = parseFloat(row.querySelector('td:nth-child(4)').textContent.replace('%', '')) / 100;
                    const totalPriceBeforeIva = unitPrice * quantity;
                    const totalIvaAmount = totalPriceBeforeIva * ivaPercentage;
                    const totalPriceAfterIva = totalPriceBeforeIva + totalIvaAmount;

                    row.querySelector('.total-price-before-iva').textContent = totalPriceBeforeIva.toFixed(2);
                    row.querySelector('.total-iva-amount').textContent = totalIvaAmount.toFixed(2);
                    row.querySelector('.total-price-after-iva').textContent = totalPriceAfterIva.toFixed(2);
                });
            });
        });
    </script>
</head>
<body>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create Order') }}
        </h2>
    </x-slot>
<div class="container mt-5">
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <form action="{{ route('orders.insert') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="client_id">Client:</label>
            <select name="client_id" id="client_id" class="form-control" required>
                <option value="">Select Client</option>
                @foreach ($clients as $client)
                    <option value="{{ $client->id }}">{{ $client->first_name }} {{ $client->last_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>Product</th>
                    <th>Category</th>
                    <th>Unit Price</th>
                    <th>IVA (%)</th>
                    <th>Quantity</th>
                    <th>Total Price Before IVA</th>
                    <th>Total IVA Amount</th>
                    <th>Total Price After IVA</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->category->name }}</td>
                        <td>€{{ $product->price }}</td>
                        <td>{{ $product->category->iva_percentage }}%</td>
                        <td>
                            <input type="number" name="quantities[]" class="form-control" value="0" min="0" required>
                            <input type="hidden" name="products[]" value="{{ $product->id }}">
                            <input type="hidden" name="prices[]" value="{{ $product->price }}">
                        </td>
                        <td>€<span class="total-price-before-iva">0.00</span></td>
                        <td>€<span class="total-iva-amount">0.00</span></td>
                        <td>€<span class="total-price-after-iva">0.00</span></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <button type="submit" class="btn btn-primary">Create Order</button>
    </form>
</div>
</x-app-layout>
</body>
</html>
